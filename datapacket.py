from settings import *
import time
from twisted.internet import reactor

def log(msg):
    print msg
    
def encode_payload(seqnum, data):
    payload = hex(seqnum)[2:].zfill(16).decode('hex')
    return payload + data

def decode_payload(payload):
    #First 8 bytes is the sequence number
    seqnum = payload[:8]
    data = payload[8:]
    ret = {
            'seqnum' : seqnum,
            'audio' : data
    }

    return ret

def parse_datagram(data):
        data = data.decode("zlib")
        data = decode_payload(data)
        seqnum = data['seqnum']
        audio = data['audio']
        return [seqnum, audio]

class Player:
    def __init__(self, stream):
        self.stream = stream
        self.queue = ''

    def play_audio(self, seqnum, audio):
        self.queue += audio
        #print len(self.queue)
        if len(self.queue) > CHUNK * 5:
            #reactor.callInThread(self.stream.write, self.queue)
            #reactor.run()
            self.stream.write(self.queue)
            self.queue = ''

class Recorder:
    def __init__(self, stream, socket, dest):
        self.seqnum = 0
        self.stream = stream
        self.socket = socket
        self.host, self.port = dest
        
    def run(self):
        while True:
            payload = self.record_audio(self.seqnum)
            print "Sent %d bytes to %s:%d" %(len(payload), self.host, self.port)
            self.socket.transport.write(payload, (self.host, self.port))
            self.seqnum += 1

    def record_audio(self, seqnum):
        data = self.stream.read(CHUNK)
        data = encode_payload(seqnum, data)
        data = data.encode('zlib')
        return data