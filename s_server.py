import socket
from sys import argv

try:
    IP = argv[1]
except IndexError:
    IP = "127.0.0.1"

try:
    PORT = int(argv[2])
except IndexError:
    PORT = 8899

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((IP, PORT))
print "Listening on %s:%d" %(IP, PORT)
while True:
    data, addr = sock.recvfrom(2048)
    print "Received message:", data
