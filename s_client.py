import socket
from sys import argv

try:
    IP = argv[1]
except IndexError:
    IP = "127.0.0.1"

try:
    PORT = int(argv[2])
except IndexError:
    PORT = 8899


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    msg = raw_input("Message:")
    sock.sendto(msg, (IP, PORT))
    print "Sent message"
