#!/usr/bin/env python

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

from sys import argv
import pyaudio
import thread

from datapacket import encode_payload
from datapacket import parse_datagram, Player, Recorder
from settings import *
from datapacket import log

try:
	host = argv[1]
except IndexError:
	host = "172.17.128.70"

try:
	port = int(argv[2])
except:
	port = 9999

p = pyaudio.PyAudio()

in_stream = p.open(format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True)

out_stream = p.open(format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    output=True)

# stream = p.open(format=FORMAT,
# 	channels=CHANNELS,
# 	rate=RATE,
# 	input=True,
# 	frames_per_buffer=CHUNK)

class Client(DatagramProtocol):
	def __init__(self):
		self.recorder = Recorder(in_stream, self, (host, port))
		self.player = Player(out_stream)

	def startProtocol(self):
		#self.transport.connect('192.168.43.175', 9999)
		thread.start_new_thread(self.transmit, ())
	
	def transmit(self):
		seqnum = 0
		while True:
			try:
				data = self.recorder.record_audio(seqnum)
				print "==> Sending %d bytes to %s:%d" %(len(data), host, port)
				protocol.sendDatagram(data)
				seqnum += 1
			except KeyboardInterrupt:
				print("stopped sending..")
				return

	def datagramReceived(self, data, (host, port)):
		print "<== Received %d bytes from %s:%d" %(len(data), host, port)
		seqnum, audio = parse_datagram(data)
		self.player.play_audio(seqnum, audio)

	def sendDatagram(self, data):
		self.transport.write(data, (host, port))

if __name__ == '__main__':
	protocol = Client()
	t = reactor.listenUDP(0, protocol)
	reactor.run()

	in_stream.stop_stream()
	in_stream.close()

	out_stream.stop_stream()
	out_stream.close()
	
	p.terminate()
	exit()

