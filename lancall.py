#!/usr/bin/env python

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from collections import deque
import pickle
import pyaudio
import thread

from datapacket import decode_payload
from datapacket import parse_datagram, Player, Recorder
from settings import *

try:
    port = int(argv[1])
except:
    port = 9999

p = pyaudio.PyAudio()
in_stream = p.open(format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True)

out_stream = p.open(format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    output=True)


class Server(DatagramProtocol):
    def __init__(self):
        self.last_rxseqnum = 0
        self.last_txseqnum = 0
        self.queue = ''
        self.transmitting = False
        self.player = Player(out_stream)

    def datagramReceived(self, data, (host, port)):
        if not self.transmitting:
            print "Starting transmission t0 %s:%d" %(host, port)
            self.transmitting = True
            #self.transport.connect(host, port)
            thread.start_new_thread(self.transmit, (host, port))
            #self.transmit(host, port)

        rxseqnum, audio = parse_datagram(data)
        print "received rxseqnum %s and size %d bytes" %(rxseqnum.encode('hex'), len(audio))
        #Drop out of order packets
        if rxseqnum < self.last_rxseqnum:
            print "dropping " + rxseqnum
            return
        else:
            self.player.play_audio(rxseqnum, audio)
            last_rxseqnum = rxseqnum

    def transmit(self, host, port):
        recorder = Recorder(in_stream, self, (host, port))
        recorder.run()

    def sendDatagram(self, data):
        self.transport.write(data)


print "Starting Server..."
s = Server()
reactor.listenUDP(port, s)
reactor.run()

in_stream.stop_stream()
in_stream.close()

out_stream.stop_stream()
out_stream.close()

p.terminate()
