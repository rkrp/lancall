import pyaudio
import wave
from client import Client
from twisted.internet import reactor


protocol = Client()
t = reactor.listenUDP(0, protocol)
reactor.run()

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "output.wav"

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

print("* recording")

frames = []

try:
    while True:
        data = stream.read(CHUNK)
        protocol.sendDatagram(data)
except KeyboardInterrupt:
    print("* done recording")
    stream.stop_stream()
    stream.close()
    p.terminate()


